import React from 'react';
import CharacterCard from '../../components/CharacterCard/CharacterCard';
import CharacterSearch from '../../components/CharacterCard/CharacterSearch';

class CardsPage extends React.Component {
    constructor (props) {
      super(props)
      this.state = {
          rickMorty: [],
          characterCards: []
      }
    }
    
    getData(search) {
        fetch("https://rickandmortyapi.com/api/character/?name=" + search)
        .then(response => response.json())
        .then(data => {
          if(data.results){
              this.setState({ 
                characterCards: data.results
            })
          }
          else{
            this.setState({ 
              characterCards: []
            })
            console.log("No match")
          }
        })
    }
    componentDidMount(){
      this.getData("")
    }

    render(){
      let characters = null;
      if (this.state.characterCards.length > 0) {
        characters = this.state.characterCards.map(character => (
          <CharacterCard
            key={character.id}
            {... character}
            showLink={true}></CharacterCard>
        ));
      }
      else {
        characters = <p>Loading...</p>
      }

      // https://rickandmortyapi.com/api/character/?name=search

      return (
        <React.Fragment>
          <CharacterSearch onChange={(search) => {this.getData(search)}} />
          <h4>Characters</h4>
          <br />
          <div className="row">
            {characters}
          </div>
        </React.Fragment>
      )
    }
  }
  
  export default CardsPage;
import React from 'react';
import LocationCard from '../../components/CharacterCard/LocationCard';
import CharacterSearch from '../../components/CharacterCard/CharacterSearch';

class LocationsPage extends React.Component {
    constructor (props) {
      super(props)
      this.state = {
        locationCards: []
      }
    }

    getData(search) {
        fetch("https://rickandmortyapi.com/api/location/?name=" + search)
        .then(response => response.json())
        .then(data => {
          if(data.results){
              this.setState({ 
                locationCards: data.results
            })
          }
          else{
            this.setState({ 
              locationCards: []
            })
            console.log("No match")
          }
        })
    }
    componentDidMount(){
      this.getData("")
    }

    render(){
      let locations = null;
      if (this.state.locationCards.length > 0) {
        locations = this.state.locationCards.map(location => (
          <LocationCard
            key={location.id}
            {... location}
            showLink={true}></LocationCard>
        ));
      }
      else {
        locations = <p>Loading...</p>
      }

      return (
        <React.Fragment>
          <CharacterSearch onChange={(search) => {this.getData(search)}} />
          <h4>Locations</h4>
          <br />
          <div className="row">
            {locations}
          </div>
        </React.Fragment>
      )
    }
  }
  
  export default LocationsPage;

import React from 'react';
import CharacterCard from '../../components/CharacterCard/CharacterCard';

class CardPage extends React.Component {
    constructor (props) {
      super(props)
      this.state = {
          character: {}
      }
      this.getCharacterData();
    }

    getCharacterData() {
        fetch("https://rickandmortyapi.com/api/character/" + this.props.match.params.id)
        .then(response => response.json())
        .then(data => {
            this.setState({ 
                character: data
            })
        })
    }

    render(){
        let characterCard = null;

        if (this.state.character.id) {
            characterCard = (<CharacterCard
                key={this.state.character.id}
                id={this.state.character.id}
                image={this.state.character.image}
                name={this.state.character.name}
                species={this.state.character.species}
                status={this.state.character.status}
                gender={this.state.character.gender}
                location={this.state.character.location}
                origin={this.state.character.origin}
                showLink={false}></CharacterCard>);
        }
        else {
          characterCard = <p>Loading...</p>;
        }
  
        return (
          <React.Fragment>{characterCard}</React.Fragment>
        )
    }
  }
  
  export default CardPage;
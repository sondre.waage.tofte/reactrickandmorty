import React from 'react';
import LocationCard from '../../components/CharacterCard/LocationCard';

class LocationPage extends React.Component {
    constructor (props) {
      super(props)
      this.state = {
          location: {}
      }
      this.getLocationData();
    }

    getLocationData() {
        fetch("https://rickandmortyapi.com/api/location/" + this.props.match.params.id)
        .then(response => response.json())
        .then(data => {
            this.setState({ 
                location: data
            })
        })
    }

    render(){
        let locationCard = null;

        if (this.state.location.id) {
            locationCard = (<LocationCard
                key={this.state.location.id}
                id={this.state.location.id}
                name={this.state.location.name}
                type={this.state.location.type}
                dimension={this.state.location.dimension}
                showLink={false}></LocationCard>);
        }
        else {
          locationCard = <p>Loading...</p>;
        }
  
        return (
          <React.Fragment>{locationCard}</React.Fragment>
        )
    }
  }
  
  export default LocationPage;
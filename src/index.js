import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import CardsPage from './containers/CharacterCards/CardsPage';
import CardPage from './containers/CharacterCards/CardPage';
import LocationsPage from './containers/CharacterCards/LocationsPage';
import LocationPage from './containers/CharacterCards/LocationPage';
import { BrowserRouter, Route } from 'react-router-dom';

//Wrapping <App/> in <BrowserRouter inside ReactDOM.render()
//Adding route to components inside app   
ReactDOM.render(
    <BrowserRouter>
        <App>
            <Route exact path="/" component={CardsPage} />
            <Route path="/character/:id" component={CardPage} />
            <Route exact path="/location/" component={LocationsPage} />
            <Route path="/location/:id" component={LocationPage} />
        </App>
    </BrowserRouter>
 ,document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

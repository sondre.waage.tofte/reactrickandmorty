import React from 'react';
import { Link } from 'react-router-dom';

var linkButton = null;

const LocationCard = (props) => {

    linkButton = <Link to={{ propertiy:"/", pathname: '/location/' + props.id, }}
    className="nav-link">View</Link>;

    return (
    <div>
        <div className="col-xs-12 col-sm-6 col-md-4">
            <div className="card LocationCard">
                <div className="card-body">
                    <h5 className="card-title">{props.name}</h5>
                    <b>Type: </b> {props.type} <br />
                    <b>Dimension: </b> {props.dimension} <br />
                    <br />
                    {props.showLink ? linkButton : null}
                </div>
            </div>
        </div>
    </div>
    );
}

export default LocationCard;
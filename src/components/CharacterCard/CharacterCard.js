import React from 'react';
import { Link } from 'react-router-dom';

var linkButton = null;

const CharacterCard = (props) => {

    linkButton = <Link to={{ propertiy:"/", pathname: '/character/' + props.id, }}
    className="nav-link">View</Link>;

    return (
    <div>
        <div className="col-xs-12 col-sm-6 col-md-4">
            <div className="card CharacterCard">

                <img src={props.image} alt={props.name} className="card-img-top"/>

                <div className="card-body">
                    <h5 className="card-title">{props.name}</h5>
                    <b>Species: </b> {props.species} <br />
                    <b>Status: </b> {props.status} <br />
                    <b>Gender: </b> {props.gender} <br />
                    <b>Loacation: </b> {props.location.name} <br />
                    <b>Place of Origin </b> {props.origin.name} <br />
                    <br />
                    {props.showLink ? linkButton : null}
                </div>
            </div>
        </div>
    </div>
    );
}

export default CharacterCard;
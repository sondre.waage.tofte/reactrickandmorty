import React, { Component } from 'react'
//import CharacterCard from './CharacterCard';

class CharacterSearch extends Component {

    constructor(props) {
        super(props)
        this.state = {
            search: '',
        }
        this.handleClick = this.handleClick.bind(this)
        this.handleSearchChange = this.handleSearchChange.bind(this)
    }

    handleSearchChange(e){
        const input = e.target.value
        this.setState({ search: input })
    }

    handleClick() {
        if (this.props.onChange) {
            this.props.onChange(this.state.search)
        }
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <input placeholder="Search..." onChange={this.handleSearchChange} />
                    <button onClick={this.handleClick}> Show me what you got </button>
                </div>
            </div>
        )
    }
}

export default CharacterSearch;
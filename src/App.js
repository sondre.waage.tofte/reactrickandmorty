import React from 'react';
import './App.css';
import { NavLink} from 'react-router-dom';

function App(props) {

  return (
    <div className="App">
      <nav>
        <NavLink to="/">
          Characters
        </NavLink>
        <NavLink to="/location">
          Locations
        </NavLink>
      </nav>
      <header>
        <img alt="This is of rick and morty" style={{width:"100%", display:"block", margin:"0"}}
        src="https://i2.wp.com/www.pixelcrumb.com/wp-content/uploads/2016/10/RICK-AND-MORTY-BANNER.jpg?fit=1920%2C720"/>
      </header>
      {props.children}
    </div>
  );
}

export default App;
